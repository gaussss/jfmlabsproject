from jose import jwt
from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.contrib.auth import authenticate

from .util import (generate_access_token,
                    generate_refresh_token)


class SafeJWTAuthentication(BaseAuthentication):
    def authenticate(self, request):
        authorization_header = request.headers.get('Authorization')
        if not authorization_header:
            return None
        try:
            # header = 'Token xxxxxxxxxxxxxxxxxxxxxxxx'
            access_token = authorization_header.split(' ')[1]
            payload = jwt.decode(
                access_token,
                settings.SECRET_KEY,
                algorithms=[settings.JWT_ALGO]
            )
            request.META['user'] = payload['data']['user_id']
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed('access_token expired')
        except IndexError:
            raise exceptions.AuthenticationFailed('Token prefix missing')
        except:
            raise exceptions.AuthenticationFailed('Token prefix missing')

        user = User.objects.get(pk=payload["data"]['user_id'])
        if user is []:
            raise exceptions.AuthenticationFailed('User not found')
        if not user.is_active:
            raise exceptions.AuthenticationFailed('user is inactive')

        return user, None


def authentificates(email, password):
    user = User.objects.get(username=email)
    if user is None:
        raise Exception("Authentification failed")
    if not check_password(password, user.password):
        raise Exception("Incorrect password")
    data = {
        "user_id": user.pk,
        "is_admin": user.is_superuser,
    }
    access_token = generate_access_token(data)
    refresh_token = generate_refresh_token(data)

    return access_token, refresh_token

