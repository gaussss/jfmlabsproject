from datetime import timedelta
from django.utils import timezone
from jose import jwt
from django.conf import settings


def generate_access_token(user):
    access_token_payload = {
        'data': user,
        'exp': timezone.now() + timedelta(hours=settings.JWT_TOKEN_EXP),
        'iat': timezone.now(),
    }
    access_token = jwt.encode(access_token_payload,
                              settings.SECRET_KEY, algorithm=settings.JWT_ALGO).encode('utf-8')
    return access_token


def generate_refresh_token(user):
    refresh_token_payload = {
        'data': user,
        'exp': timezone.now() + timedelta(hours=settings.JWT_REFRESH_EXP),
        'iat': timezone.now()
    }
    refresh_token = jwt.encode(
        refresh_token_payload, settings.SECRET_KEY, algorithm=settings.JWT_ALGO).encode('utf-8')
    return refresh_token
