from unittest import result
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from .serializers import ImageSerializer
from .models import Image
from .custom_viewset import CRUDViewset
from .custom_responses import GoodResponse, BadRequest
from .authentification import authentificates

import base64
from django.core.files.base import ContentFile


from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.applications import imagenet_utils
from tensorflow.keras.models import load_model
import tensorflow as tf
import numpy as np
from nudenet import NudeClassifier


@api_view(['POST'])
@permission_classes([AllowAny])
def login_view(request):
	try:
		access_token, refresh_token = authentificates(email=request.data['email'], password=request.data['password'])
	except Exception as e:
		return BadRequest(e)
	data = {'access': access_token, 'refresh': refresh_token}
	return GoodResponse(data)


def predict_fu(path,model):
  inputShape = (299, 299)
  img = load_img(path,target_size=inputShape)
  image = img_to_array(img)
  image = np.expand_dims(image,axis=0)
  image = preprocess_input(image)
  
  predicts = model.predict(image)
  P = imagenet_utils.decode_predictions(predicts)
  result = {}
  for imagenetID, label, prob in P[0]:
      result[label] = prob
      # print("{}. {}: {:.2f}%".format(i + 1, label, prob * 100))
      # return imagenetID,label
  return result

model = InceptionV3(weights='imagenet')
classifier = NudeClassifier()

class ImageViewSet(CRUDViewset):
  url_prefix = 'detect'
  queryset = Image.objects.all()
  serializer_class = ImageSerializer
  permission_classes = [IsAuthenticated]

  def create(self, request, *args, **kwargs):
      files = request.FILES.getlist('file')
      result = []
      for file in files:
        data = {}
        data['file'] = file
        name = file.__dict__['_name']
        image = Image.objects.create(**data)
        file_path = './media/' + str(image.file)
        predictions = predict_fu(file_path, model)
        nudity = classifier.classify(file_path)
        result.append({
          'name': name,
          'labels': predictions,
          'nudity': nudity[file_path]
        })

      return GoodResponse(result)

