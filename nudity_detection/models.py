from django.db import models


class Image(models.Model):
    description = models.TextField(default='Aucune description')
    file = models.FileField(upload_to="files")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
