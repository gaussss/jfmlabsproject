"""jfmlabsAI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from . import views as v
from django.urls import path
from rest_framework.routers import SimpleRouter

viewsets = (v.ImageViewSet, )

router = SimpleRouter()
for viewset in viewsets:
    router.register(r'{}'.format(viewset.url_prefix), viewset, basename=viewset.url_prefix)

urlpatterns = [
    path('token/', v.login_view, name='token_obtain'),
]
urlpatterns += router.urls
