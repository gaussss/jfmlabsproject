from rest_framework.response import Response
from rest_framework import status


class CResponse(Response):
    
    def __init__(self, msg, statut):
        Response.__init__(
            self,
            {
                "detail": msg,
                "status": statut
            },
            statut
        )


class BadRequest(CResponse):
    
    def __init__(self, exception_instance):
        CResponse.__init__(self, repr(exception_instance), status.HTTP_400_BAD_REQUEST)


class UnAutorizedRequest(CResponse):
    
    def __init__(self, exception_instance):
        CResponse.__init__(self, repr(exception_instance), status.HTTP_401_UNAUTHORIZED)


class GoodResponse(CResponse):
    
    def __init__(self, msg):
        CResponse.__init__(self, msg, status.HTTP_200_OK)
